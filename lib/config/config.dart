abstract class Config {
  static const BASE_URL = 'https://api.themoviedb.org/';
  static const IMG_BASE_URL = 'https://image.tmdb.org/';
}
