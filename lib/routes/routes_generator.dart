import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/auth/login_page.dart';
import 'package:majootestcase/ui/auth/register_page.dart';
import 'package:majootestcase/ui/details/details_page.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/home/home_page.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/register':
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => AuthBlocCubit(),
            child: RegisterPage(),
          ),
        );
      case '/login':
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => AuthBlocCubit()..fetchHistoryLogin(context),
            child: LoginPage(),
          ),
        );
      case '/home':
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => HomeBlocCubit()..fetchingData(),
            child: HomePage(),
          ),
        );
      case '/error':
        return MaterialPageRoute(
          builder: (context) => ErrorScreen(),
        );
      case '/detail':
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => HomeBlocCubit()..fetchingData(),
            child: DetailPage(),
          ),
        );

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Error")),
        body: Center(child: Text('Error page')),
      );
    });
  }
}
