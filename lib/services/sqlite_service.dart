import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class SqliteService {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();

    return openDatabase(
      join(path, 'database.db'),
      onCreate: (database, version) async {
        await database.execute(
          "CREATE TABLE Users(id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)",
        );
      },
      version: 1,
    );
  }

  Future<void> createItem(User user) async {
    final Database db = await initializeDB();
    await db.insert(
      'Users',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<String> userCek(String email, String password) async {
    final Database db = await initializeDB();
    var result = await db.rawQuery(
        "SELECT COUNT(id) 'jumlah' FROM Users WHERE email='" +
            email +
            "' AND password='" +
            password +
            "'");
    int response = result[0]['jumlah'];
    String res;
    if (response != 0) {
      res = "ok";
    } else {
      res = "bad";
    }
    return res;
  }
}
