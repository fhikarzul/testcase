import 'dart:async';
import 'package:dio/dio.dart';
import 'package:majootestcase/config/config.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance;
createInstance() async {
  var option = BaseOptions(
      baseUrl: Config.BASE_URL +
          "3/trending/all/day?api_key=ceda83ac004082618c659e378ae526e4",
      connectTimeout: 60 * 1000,
      receiveTimeout: 60 * 1000);

  dioInstance = new Dio(option);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  dioInstance.options.baseUrl = Config.BASE_URL +
      "3/trending/all/day?api_key=ceda83ac004082618c659e378ae526e4";
  return dioInstance;
}
