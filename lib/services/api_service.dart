import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Dio dio;

  Future<MovieModel> getMovieList() async {
    try {
      var dioConf = await dioConfig.dio();

      var response = await Dio().get(dioConf.options.baseUrl);
      return MovieModel(
          page: response.data['page'],
          results: response.data['results'],
          totalPages: response.data['total_pages'],
          totalResults: response.data["total_results"]);
    } on DioError catch (ex) {
      if (ex.type == DioErrorType.connectTimeout) {
        return null;
      } else if (ex.type == DioErrorType.receiveTimeout) {
        return null;
      }
      return null;
    }
  }
}
