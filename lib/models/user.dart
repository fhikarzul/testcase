class User {
  String username;
  String email;
  String password;

  User({this.username, this.email, this.password});

  User.fromMap(Map<String, dynamic> item)
      : username = item["username"],
        email = item["email"],
        password = item["password"];

  Map<String, Object> toMap() {
    return {
      'username': username,
      'email': email,
      'password': password,
    };
  }
}
