import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:majootestcase/ui/auth/login_page.dart';
import 'package:majootestcase/ui/splash/splash_page.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'routes/routes_generator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (context) => AuthBlocCubit()..fetchHistoryLogin(context),
        child: SplashPage(),
      ),
      builder: EasyLoading.init(),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
      print("Fhikar: " + state.toString());
      if (state is AuthBlocLoginState) {
        return LoginPage();
      } else if (state is AuthBlocLoggedInState) {
        Navigator.of(context).pushNamed("/home");
      }

      return Center(child: Text("lOADING"));
    });
  }
}
