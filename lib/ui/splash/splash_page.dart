import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: BlocBuilder<AuthBlocCubit, AuthBlocState>(
        builder: (context, state) {
          Timer(
            const Duration(seconds: 3),
            () {
              if (state is AuthBlocLoginState) {
                Navigator.pushNamedAndRemoveUntil(
                    context, "/home", (Route<dynamic> route) => false);
              }
              if (state is AuthBlocLoggedInState) {
                Navigator.pushNamedAndRemoveUntil(
                    context, "/login", (Route<dynamic> route) => false);
              }
            },
          );

          return Container(
            alignment: Alignment.center,
            child: const Image(
              height: 300,
              width: 300,
              image: AssetImage('assets/logo.png'),
            ),
          );
        },
      ),
    );
  }
}
