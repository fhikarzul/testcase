import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  bool _isObscurePassword = true;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  @override
  Widget build(context) {
    return Scaffold(
      body: BlocBuilder<AuthBlocCubit, AuthBlocState>(
        builder: (context, state) {
          if (state is AuthBlocLoadingState) {
            EasyLoading.show(status: "Memuat");
          }
          if (state is AuthBlocFailedState) {
            EasyLoading.showToast(state.msg);
          }
          if (state is AuthBlocSuccesState) {
            EasyLoading.showToast(state.msg);
          }
          return Container(
            color: Colors.blue,
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 50, left: 10),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    "Login.",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 32),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 50),
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(20),
                          topLeft: Radius.circular(20)),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                              margin: const EdgeInsets.only(top: 50),
                              child: const Center(
                                child: Image(
                                  image: AssetImage('assets/logo.png'),
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.all(20),
                            child: _formLogin(),
                          ),
                          _loginButton(context),
                          _registerButton(),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _loginButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Card(
        color: Colors.blue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: InkWell(
          onTap: () {
            context.read<AuthBlocCubit>().loginUser(
                context, _emailController.value, _passwordController.value);
          },
          child: Container(
            padding:
                const EdgeInsets.only(left: 50, right: 50, top: 15, bottom: 15),
            child: const Text(
              "Login",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
          ),
        ),
      ),
    );
  }

  Widget _registerButton() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.pushNamed(context, '/register');
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Register',
                ),
              ]),
        ),
      ),
    );
  }

  Widget _formLogin() {
    return Column(
      children: [
        CustomTextFormField(
          context: context,
          controller: _emailController,
          isEmail: true,
          hint: 'Example@123.com',
          label: 'Email',
        ),
        CustomTextFormField(
          context: context,
          label: 'Password',
          hint: 'password',
          controller: _passwordController,
          isObscureText: _isObscurePassword,
          suffixIcon: IconButton(
            icon: Icon(
              _isObscurePassword
                  ? Icons.visibility_off_outlined
                  : Icons.visibility_outlined,
            ),
            onPressed: () {
              setState(() {
                _isObscurePassword = !_isObscurePassword;
              });
            },
          ),
        ),
      ],
    );
  }
}
