import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.wifi_tethering_error_sharp,
              color: Colors.white,
              size: 100,
            ),
            Text(
              "Terjadi Kesalahan Pada Jaringan!",
              style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            _refreshButton(context),
          ],
        ),
      ),
    );
  }

  Widget _refreshButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Card(
        color: Colors.blue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: InkWell(
          onTap: () {
            Navigator.pushNamedAndRemoveUntil(
                context, "/home", (Route<dynamic> route) => false);
          },
          child: Container(
            padding:
                const EdgeInsets.only(left: 50, right: 50, top: 15, bottom: 15),
            child: const Text(
              "REFRESH",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
          ),
        ),
      ),
    );
  }
}
