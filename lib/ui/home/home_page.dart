import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/config/config.dart';
import 'package:majootestcase/ui/details/details_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.home),
        title: Text('Beranda'),
        actions: [
          InkWell(
            onTap: () {
              context.read<HomeBlocCubit>().logOut(context);
            },
            child: Icon(Icons.logout_sharp),
          )
        ],
      ),
      body: BlocBuilder<HomeBlocCubit, HomeBlocState>(
        builder: (context, state) {
          if (state is HomeBlocErrorState) {
            EasyLoading.dismiss();

            Future.delayed(
              Duration.zero,
              () {
                Navigator.pushNamedAndRemoveUntil(
                    context, "/error", (Route<dynamic> route) => false);
              },
            );
          }
          if (state is HomeBlocLoadingState) {
            EasyLoading.show(status: "Memuat");
          }
          if (state is HomeBlocLoadedState) {
            EasyLoading.dismiss();

            return GridView.count(
              padding: EdgeInsets.all(10),
              crossAxisCount: 2,
              children: List.generate(
                state.data.results.length,
                (index) {
                  return Card(
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: InkWell(
                      onTap: () async {
                        Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder:
                                  (context, animation, secondaryAnimation) =>
                                      DetailPage(),
                              settings: RouteSettings(arguments: {
                                'image': state
                                    .data.results[index]['poster_path']
                                    .toString(),
                                'title': state.data.results[index]
                                            ['original_title'] !=
                                        null
                                    ? state
                                        .data.results[index]['original_title']
                                        .toString()
                                    : state.data.results[index]['name']
                                        .toString(),
                                'tahun': state
                                    .data.results[index]['release_date']
                                    .toString(),
                                'rating': state
                                    .data.results[index]['vote_average']
                                    .toString(),
                                'overview': state
                                    .data.results[index]['overview']
                                    .toString(),
                                'popularity': state
                                    .data.results[index]['popularity']
                                    .toString(),
                              }),
                            ));
                      },
                      child: Column(
                        children: [
                          AspectRatio(
                            aspectRatio: 487 / 360,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                  fit: BoxFit.fitWidth,
                                  alignment: FractionalOffset.topCenter,
                                  image: NetworkImage(
                                    Config.IMG_BASE_URL +
                                        "t/p/w600_and_h900_bestv2" +
                                        state.data.results[index]
                                            ['poster_path'],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(top: 10, left: 10, right: 10),
                            child: Text(
                              state.data.results[index]['original_title'] !=
                                      null
                                  ? state.data.results[index]['original_title']
                                  : state.data.results[index]['name']
                                      .toString(),
                              style: const TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 13),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          }
          return Container(
            child: Center(child: Text("Memuat...")),
          );
        },
      ),
    );
  }
}
