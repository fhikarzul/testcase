import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  MovieModel movieModel;
  int index;

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    ApiServices apiServices = ApiServices();
    movieModel = await apiServices.getMovieList();
    if (movieModel != null) {
      emit(HomeBlocLoadedState(movieModel));
    } else {
      emit(HomeBlocErrorState("Error Unknown"));
    }
  }

  void logOut(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    Navigator.pushNamedAndRemoveUntil(
        context, "/login", (Route<dynamic> route) => false);
  }
}
