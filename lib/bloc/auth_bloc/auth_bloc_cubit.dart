import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/sqlite_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  SqliteService _sqliteService = SqliteService();

  void fetchHistoryLogin(BuildContext context) async {
    _getSession().then((value) {
      if (value != null) {
        if (value == true) {
          emit(AuthBlocLoginState());
        } else {
          emit(AuthBlocLoggedInState());
        }
      } else {
        emit(AuthBlocLoggedInState());
      }
    });
  }

  void loginUser(BuildContext context, email, String password) async {
    emit(AuthBlocLoadingState());
    if ((email != "" && password != "") &&
        (email != null && password != null)) {
      if (emailValidator(email)) {
        _sqliteService.userCek(email, password).then((value) async {
          if (value == "ok") {
            emit(AuthBlocSuccesState("Login Berhasil"));
            _setSession().whenComplete(
              () {
                Navigator.pushNamedAndRemoveUntil(
                    context, "/home", (Route<dynamic> route) => false);
              },
            );
          } else {
            emit(AuthBlocFailedState(
                "Login gagal, periksa kembali inputan anda"));
          }
        });
      } else {
        emit(AuthBlocFailedState("Masukkan e-mail yang valid"));
      }
    } else {
      emit(AuthBlocFailedState(
          "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"));
    }
  }

  void registerUser(
      BuildContext context, String username, String email, String password) {
    emit(AuthBlocLoadingState());
    if ((username != "" && email != "" && password != "") &&
        (username != null && email != null && password != null)) {
      if (emailValidator(email)) {
        _sqliteService.initializeDB().whenComplete(
          () async {
            User user = User(
              username: username,
              email: email,
              password: password,
            );
            await _sqliteService.createItem(user);
            emit(AuthBlocSuccesState("Register Berhasil"));
            _setSession().whenComplete(
              () {
                Navigator.pushNamedAndRemoveUntil(
                    context, "/home", (Route<dynamic> route) => false);
              },
            );
          },
        );
      } else {
        emit(AuthBlocFailedState("Masukkan e-mail yang valid"));
      }
    } else {
      emit(AuthBlocFailedState(
          "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"));
    }
  }

  Future<void> _setSession() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    sharedPreferences.setBool("is_logged_in", true);
  }

  Future<bool> _getSession() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    return isLoggedIn;
  }

  bool emailValidator(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }
}
