part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSuccesState extends AuthBlocState {
  final msg;
  AuthBlocSuccesState(this.msg);
}

class AuthBlocFailedState extends AuthBlocState {
  final msg;
  AuthBlocFailedState(this.msg);
}
